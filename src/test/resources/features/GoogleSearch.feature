Feature: Print Suggestions
  In order to print Suggestions
  As a Quality Engineer
  I want to look up word Suggestions

  Scenario: Verify the suggestions for a given search
    Given navigate to Google home page
    When  search for the text "automation"
    Then verify that all the suggestion contains "automation"
package stepDefinitions.GoogleStepDefs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import pages.GooglePages.GoogleHomePage;

public class GoogleSteps {

    @Steps
    GoogleHomePage googleHomePage;

    @Given("navigate to Google home page")
    public void navigate_to_Google_home_page() {
        googleHomePage.openUrl("https://www.google.com");
    }


    @When("search for the text {string}")
    public void search_for_the_text(String string) {
        googleHomePage.inputSearchbar(string);
    }


    @Then("verify that all the suggestion contains {string}")
    public void verify_that_all_the_suggestion_contains(String string) {
        googleHomePage.verifyTheSuggestions(string);

    }
}

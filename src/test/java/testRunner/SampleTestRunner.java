package testRunner;


import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        glue = {"stepDefinitions"},
        features = "src/test/resources/features",
        dryRun = false
)
public class SampleTestRunner {


}

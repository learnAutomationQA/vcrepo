package pages.GooglePages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class GoogleHomePage extends PageObject {


    @FindBy(xpath="//input[@name='q']")
    private WebElementFacade inputSearchBar;

    @FindBy(css="ul li div[role] > *:first-child>span")
    private List<WebElementFacade> listSuggestions;


    public void inputSearchbar(String searchParam){
        inputSearchBar.type(searchParam);
    }

    public void verifyTheSuggestions(String expectedString){
        for(WebElementFacade suggestion : listSuggestions){
            Assert.assertTrue("The test case failed as the suggestion does not contain the text we are expecting"
                    ,suggestion.getText().contains(expectedString));
        }
    }


}